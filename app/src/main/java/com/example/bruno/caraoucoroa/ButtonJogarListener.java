package com.example.bruno.caraoucoroa;

import android.content.Intent;
import android.view.View;

/**
 * Created by bruno on 14/02/18.
 */

public class ButtonJogarListener implements View.OnClickListener {

    @Override
    public void onClick(View view) {
        Moeda moeda = Moeda.gerarMoeda();

        Intent intent = new Intent(view.getContext(), DetailActivity.class);
        intent.putExtra("moeda", moeda);

        view.getContext().startActivity(intent);
    }
}
