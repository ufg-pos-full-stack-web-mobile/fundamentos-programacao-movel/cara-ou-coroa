package com.example.bruno.caraoucoroa;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class DetailActivity extends AppCompatActivity {

    private ImageView imagem;
    private ImageView botaoJogarNovamente;
    private ImageView botaoVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imagem = findViewById(R.id.image_coin);
        botaoVoltar = findViewById(R.id.button_back);
        botaoJogarNovamente = findViewById(R.id.button_play_again);

        botaoVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        botaoJogarNovamente.setOnClickListener(new ButtonJogarNovamenteListener(imagem));

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            Moeda moeda = (Moeda) extras.get("moeda");

            imagem.setImageDrawable( ContextCompat.getDrawable(this, moeda.getDrawable()) );
        }

    }
}
