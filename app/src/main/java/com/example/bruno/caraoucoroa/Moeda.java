package com.example.bruno.caraoucoroa;

import java.util.Random;

/**
 * Created by bruno on 14/02/18.
 */

public enum Moeda {
    CARA(0, R.drawable.moeda_cara),
    COROA(1, R.drawable.moeda_coroa);

    private final int codigo;
    private final int drawableCod;

    Moeda(int codigo, int drawableCod) {
        this.codigo = codigo;
        this.drawableCod = drawableCod;
    }

    public static Moeda gerarMoeda() {
        Random random = new Random();
        int codigoRandom = random.nextInt(Moeda.values().length);

        return getByCodigo(codigoRandom);
    }

    public static Moeda getByCodigo(int codigo) {
        for (Moeda moeda : Moeda.values()) {
            if (moeda.codigo == codigo) {
                return moeda;
            }
        }
        return null;
    }

    public int getDrawable() {
        return drawableCod;
    }
}
