package com.example.bruno.caraoucoroa;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by bruno on 15/02/18.
 */
public class ButtonJogarNovamenteListener implements View.OnClickListener {

    private final ImageView imagem;

    public ButtonJogarNovamenteListener(ImageView imagem) {
        this.imagem = imagem;
    }

    @Override
    public void onClick(View v) {
        Moeda moeda = Moeda.gerarMoeda();

        imagem.setImageDrawable( ContextCompat.getDrawable(v.getContext(), moeda.getDrawable()) );
    }
}
